from django.shortcuts import render
from .models import Status
from .forms import statusForm
import datetime

# Create your views here.
def landing(request):
    if request.method == "POST":
        form = statusForm(request.POST)
        if form.is_valid():
            post = form.save()
            post.author = request.user
            post.published_date = datetime
            post.save()
        else:
            try:
                pk = request.POST['id']
                obj = Status.objects.get(pk=pk)
                obj.delete()
            except Status.DoesNotExist:
                obj = None


    form = statusForm()
    value = Status.objects.all()
    return render(request, "landing.html", {"form" : form, "value" : value })
