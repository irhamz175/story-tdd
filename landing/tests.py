from django.test import TestCase,Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import *
import unittest
import time

# Create your tests here.
class uniTests(TestCase):

    def test_ada_url(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)

    def test_ada_fungsi(self):
        response = resolve("/")
        self.assertEqual(response.func,landing)

    def test_ada_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response,"landing.html")

    def test_ada_form(self):
        response = Client().get("")
        content = response.content.decode("utf8")
        self.assertIn("<form",content)

    def test_ada_button(self):
        response = Client().get("")
        content = response.content.decode("utf8")
        self.assertIn("<button",content)

    # def test_model_nya(self):
    #     status.create(status = "ey b0ss")
    #     self.assertEqual(1,status.objects.all().count())
    #     self.assertNotEqual(0,status.objects.all().count())

    def buat_model(self, status="ey b0ss"):
        return Status.objects.create(status = status)

    def test_buat_model(self):
        w = self.buat_model()
        self.assertEqual(1,Status.objects.all().count())
        self.assertNotEqual(0,Status.objects.all().count())

class functionalTests(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_masukin_form_coba_coba(self):
        self.browser.get("http://127.0.0.1:8000/")
        time.sleep(5)

        search_box = self.browser.find_element_by_id("id_status")
        search_box.send_keys("Coba Coba")
        search_box.submit()

        time.sleep(2)
        self.assertIn("Coba Coba", self.browser.page_source)
