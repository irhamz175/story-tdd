from django import forms
from .models import Status

class statusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']

    def __init__(self, *args, **kwargs):
        super(statusForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
