from django.db import models

# Create your models here.
class Status(models.Model):
    status = models.CharField(max_length=300, primary_key=True)
    waktuPost = models.DateField(auto_now = True)
